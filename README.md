# 2024-UniDive-Naples

Welcome to the training session designed for the new structure of Gitlab branches of PARSEME. 


## Getting started

From now on, all language repositories are organized into two primary branches: `master` (or `main`) and `dev`. As members of a language team, you are authorized to make changes exclusively to the `dev` branch, while the `master` branch is unauthorized for direct modifications. For more details, please refer to the document [Managing branches in the git repository of your language](https://gitlab.com/parseme/corpora/-/wikis/Managing-branches-in-the-git-repository-of-your-language).

Whenever you push to your `dev` branch, a **validation pipeline** is run on your `.cupt` files to check that their format is correct. This tutorial is a quick-start demonstration of this pipeline. 

To get started, create a fork in your personal namespace:
1. On the project’s homepage (https://gitlab.com/parseme/training/2024-unidive-naples), in the upper-right corner, select `Fork`
2. As `Project URL`, select the personal namespace your fork should belong to.
3. Click on `Fork project`.

The repository will be located in your Gitlab personal account.

## Trigger the validation pipeline (in a bash console)

1. Clone the repository
```bash
git clone git@gitlab.com:[namespace]/2024-unidive-naples.git
cd 2024-unidive-naples
```

2. Switch to the `dev` branch
```bash
git checkout dev
```

3. The pipeline is trigerred when events like push or merge request take place. To test it, you need to make a chance to your repository, commit it and push.
```bash
git mv not_to_release/toy.cupt .
git commit -m "add toy.cupt" -a
git push origin dev
```

4. To see outcomes of the pipeline, go to the web interface of your Gitlab repository and click on Build -> Pipelines.  
The most recent commit is diplayed on top of the list, and its status is denoted by `Passed` or `Failed` indicator (i.e. an icon with a green check character - for the former - or red cross - for the latter). For detailed insights, one may simply click on the respective indicator to view the following:

    ![Gitlab pipeline level 1](not_to_release/images/level_1.png)

## Fix the validation level 1

At this level, the validator exclusively tests the order of lines, newline encoding, and conducts core tests to ensure the file's integrity. It invokes the [UD validator](https://universaldependencies.org/validation-rules.html) at level 1 and supplements it with new tests designed for the CUPT format. See detailed informations in [File format validation](https://gitlab.com/parseme/corpora/-/wikis/parseme-tools#file-format-validation).

As demonstrated in the preceding figure, the job `test_cupt_level_1` did not execute successfully. Click on this job to review errors:
```bash
[Line 1]: [L1 Format invalid-first-line] Spurious first line: '# global.colmns = ID FORM LEMMA UPOS XPOS FEATS HEAD DEPREL DEPS MISC PARSEME:MWE
'. First line must specify global.columns
```

Correct this error (the first line should be entitled `global.columns` instead of `global.colmns`), then commit and push your changes:
```bash
git commit -m "Correcting 1st line in toy.cupt" -a
git push origin dev
```
As a result, the job `test_cupt_level_1` should pass.

## Fix the validation level 2

At level 2, the script uses the [UD validator](https://universaldependencies.org/validation-rules.html) (level 2) for morphosyntax vlidation, and implements additional tests for the PARSEME content, such as a syntax check for the `PARSEME:MWE` column. See detailed informations in [File format validation](https://gitlab.com/parseme/corpora/-/wikis/parseme-tools#file-format-validation).

Click on the newly generated pipeline after addressing the issue at validation level 1, you will observe that the job `test_cupt_level_2` failed with this error:

```bash
[Line 67]: [L2 MWE redefined-mwe-code] MWE code was redefined ('2:VPC.full' => '2:LVC.full')
MWE errors: 1
```

Correct this error (_set up_ has identifier 2, so _pay attention_ must have identifier 3), then commit and push your changes. Consequently, the job `test_cupt_level_2` will execute successfully, leading to the successful completion of the entire pipeline.

Congratulations! You have successfully completed this training session.

